demono
======

Simple and small Unix daemon library.

License: MIT.


Features
========

* Very simple

Installation
============

From source:

* ``git clone git://github.com/Mezozoysky/demono.git``
* ``cd demono``
* ``python3 setup.py install``

Requirements
============

* Python 3

Note: Click 6.x required for running sample daemons only.

Limitations
===========

* Unix only

Getting Help
============
    
* Email me: <mezozoysky@gmail.com>
* Post `issues to GitHub <http://github.com/Mezozoysky/demono/issues>`

